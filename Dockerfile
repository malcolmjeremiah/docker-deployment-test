FROM ruby:alpine

ENV RAILS_ENV ${RAILS_ENV:-development}

RUN apk add --no-cache sqlite-libs
RUN apk add --no-cache --virtual .build-deps \
  build-base \
  sqlite-dev
  # postgresql-dev

RUN apk add --no-cache \
  # nodejs \
  # yarn \
  tzdata \
  libpq

# NOTE: Uncomment after generate rails app
RUN bundle config --global frozen 1

WORKDIR /app

COPY . /app

RUN bundle lock --add-platform x86_64-linux-musl

RUN bundle install --jobs $(expr $(nproc --all) - 1)
# RUN yarn install

# NOTE: Uncomment after generate rails app
RUN apk del .build-deps

EXPOSE 3000

# ENTRYPOINT [ "/bin/sh" ]
CMD [ "rails", "server", "-b", "0.0.0.0", "-e", "$RAILS_ENV" ]
# CMD bundle exec rails server -b 0.0.0.0 -e $RAILS_ENV